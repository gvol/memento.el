;;; memento.el --- A package to remind you of the short time you have   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Ivan Andrus

;; Author: Ivan Andrus <darthandrus@gmail.com>
;; URL: https://gitlab.com/gvol/memento.el
;; Version: 0.0.1
;; Package-Requires: ((cl-lib "0.5"))
;; Keywords: games, help

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; "Memento Mori," or consciously remembering one's own death has a long
;; history in Western philosophical thought.  It is a way of reminding oneself
;; that death is inevitable, thereby increasing gratitude for the time left,
;; and motivation to live to the fullest.
;;
;; Death, of course, is not the only ending, and so this package
;; allows you to choose what to be reminded of.  Every day it will
;; choose a new memento randomly and put it into `global-mode-string'
;; which is usually displayed in the mode line.

;;; Code:

(require 'cl-macs)

;;;###autoload
(defgroup memento
  '((memento-mementos custom-variable))
  "Reminders of your limited time."
  :group 'help)

;;;###autoload
(defcustom memento-mementos
  '(("%y years since Harrison Bergeron was published"
     :since "1969-10-01")
    ("%y years until Harrison Bergeron becomes fully equal"
     :until "2081-03-14")
    ("%.5W weeks until Y2038"
     :until "2038-01-19 03:14:07")
    ("%.5Y years since Y2K"
     :since "2000-01-01")
    ("%f lunar cycles since man set foot on the moon"
     :since "1969-07-20 20:17:40"
     :formula (lambda (days) (/ days 29.5))))
  "List of mementos, namely things that will remind you of your short time.

Each memento is a list, the first element of which is a format
string.  The rest is a plist, namely a list of alternating
key/value pairs.  The supported keys are

:until  A time specification indicating the memento will happen in
        the future.
:since  A time specification indicating the memento happened in
        the past.
:formula A lambda taking one argument: the number of days as a
        float.  This is useful when the reminder depends on
        something other than simple time intervals, such as the
        number of books you might read before you die.  The
        lambda can return an already formatted string, or a float
        which will then be formatted below.

One of `:until' or `:since' is required to determine the time
between now and the memento.

The format string understands the `format'-like substitution
sequences below.  They also support all the flags supported by
`format-spec', such as padding and truncating to a given width.

%y  The number of years truncated to an integer.
%Y  The number of years as a float.
%m  The number of months truncated to an integer.
%M  The number of months as a float.
%w  The number of months truncated to an integer.
%W  The number of months as a float.
%d  The number of months truncated to an integer.
%D  The number of months as a float.
%f  The result of the `:formula' truncated to an integer.
%F  The result of the `:formula' as a float.

As shown, most sequences have a upper case and a lower case
version.  The lower case versions are truncated to integers since
this is a common use case.  The upper case versions return a
float for full control, but will likely require a width or
precision specifier in practice, such as \"%.2Y\" to show the
number of years with two decimal points of precision."
  :group 'memento
  :type '(repeat (cons
                  (string :tag "Format string")
                  (plist :key-type symbol
                         :options ((:until string :tag "Date")
                                   (:since string) (:formula function))
                         :value-type string))))

;;;###autoload
(defcustom memento-display-in-modeline t
  "If non-nil, `memento-mode' will add mementos to the modeline.
Really, it adds it to the `global-mode-string', which usually
appears in the modeline, however, if you have customized your
modeline it may not appear."
  :group 'memento
  :type 'boolean)

(defvar memento-string ""
  "Holds the formatted memento string.
Not intended to be set directly, as it will be overriden
periodically.  There should be no harm in doing so, however.")

(defvar memento--modeline-construct
  '(:eval (concat memento-string " "))
  "Mode line construct to append a space to the `memento-string'.
This is considered best practice for inclusion in `global-mode-string'.")

(defun memento--parse-time (value)
  "Parse a time VALUE into a format usable by memento.
Really this is just a convenience function around `parse-time-string',
which see for details on supported format, etc."
  (if (stringp value)
      (cl-destructuring-bind (sec min hour day month year _ _ _)
          (parse-time-string value)
        (unless (and day month year)
          (error "Unable to parse as date: %s" value))
        (encode-time (or sec 0) (or min 0) (or hour 0) day month year))
    (error "Unable to parse as date: %s" value)))

(defun memento-format-memento (memento)
  "Format MEMENTO based on the current time."
  (let* ((format-string (car memento))
         (args-plist (cdr memento))
         (until-value (plist-get args-plist :until))
         (since-value (plist-get args-plist :since))
         (current-time-list nil)
         (seconds (float-time
                   (if until-value
                       (time-subtract (memento--parse-time until-value)
                                      (current-time))
                     (time-subtract (current-time)
                                    (memento--parse-time since-value)))))
         (days (/ seconds (* 60 60 24)))
         (weeks (/ days 7))
         (months (/ days 30))
         (years (/ days 365.2425))
         (formula (plist-get args-plist :formula))
         (formula-value (when (functionp formula)
                          (funcall formula days))))
    (when (not (or until-value since-value))
      (error ":since or :until required"))
    (format-spec format-string
                 `((?Y . ,years)
                   (?y . ,(truncate years))
                   (?M . ,months)
                   (?m . ,(truncate months))
                   (?W . ,weeks)
                   (?w . ,(truncate weeks))
                   (?D . ,days)
                   (?d . ,(truncate days))
                   (?F . ,formula-value)
                   (?f . ,(if (numberp formula-value)
                              (truncate formula-value)
                            formula-value)))
                 'ignore)))

(defun memento--random-memento ()
  "Randomly choose a memento from `memento-mementos'."
  (nth (random (length memento-mementos)) memento-mementos))

(defun memento--set-memento-string ()
  "Set `memento-string' to a random memento.
Meant to be called from a timer."
  (setq memento-string (memento-format-memento (memento--random-memento))))

(defun memento--add-to-modeline ()
  "Add `memento-string' to `global-mode-string' for display in the modeline."
  (when memento-display-in-modeline
    (add-to-list 'global-mode-string memento--modeline-construct)))

;;;###autoload
(define-minor-mode memento-mode
    "Display a memento in the `global-mode-string' and update daily.

The variable `memento-string' can be used to display in other
places as desired.

With a prefix argument ARG, enable Memento mode if ARG is
positive, and disable it otherwise.  If called from Lisp, enable
it if ARG is omitted or nil."
  :global t
  :group 'memento
  (cancel-function-timers #'memento--set-memento-string)
  (when memento-mode
    (run-at-time "00:00" (* 60 60 24) #'memento--set-memento-string)
    (memento--set-memento-string))
  (if memento-mode
      (memento--add-to-modeline)
    (setq global-mode-string
          (delete memento--modeline-construct
                  (or global-mode-string '(""))))))

(provide 'memento)

;;; memento.el ends here
