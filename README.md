# Memento Emacs package

**This package is no longer maintained.**

**You should use [emacs-memento-mori](https://github.com/gvol/emacs-memento-mori) which has all features of this package and is avaiable [on MELPA](https://melpa.org/#/memento-mori),**

## Introduction

"Memento Mori," or consciously remembering one's own death, has a long
history in Western philosophical thought.  It is a way of reminding
oneself that death is inevitable, thereby increasing gratitude for the
time left, and motivation to live to the fullest.

Death, of course, is not the only ending, and so this package
allows you to choose what to be reminded of.  Every day it will
choose a new memento randomly and put it into `global-mode-string'
which is usually displayed in the mode line.

## Install

I hope to soon make this available via MELPA (or perhaps even GNU
ELPA), but until then you can simply copy this file to some location
in your `load-path` and add

```
(require 'memento)
```

to your `.emacs` file.  If you are using `use-package` or another
system I assume you can figure out the proper incantation.  It
shouldn't need any special handling.

## Customization

This package will likely not be useful to anyone out of the box.  To
have a meaningful experience you should customize `memento-mementos`.
You can do this by running `M-x customize-group RET memento RET`.
While it's unlikely they will have the desired psychological impact,
there are a few example items which you can refer to when creating
your own.

Some questions to consider:
* How long until you will "likely" die?  I used https://population.io/
  to get an estimate of when I will die based on just birth date,
  country, and gender.  Undoubtedly, a better estimate could be had
  with more information (e.g. about general health), but it was simple
  and it's only an estimate anyway.
* How long until your child(ren) will leave home?
* How many more vacations you will take (with your children)?
* How long until your (grand)parents die?
* How long until you are no longer a 20-something?
* How long until you reach "peak unhappiness" ([47.2 years old](https://link.springer.com/article/10.1007/s00148-020-00797-z))?
* How long has it been since some traumatic experience?

Consider using weeks or fractional years.  That way you can see the
number change every so often and be reminded how short life actually
is.  This package is not meant to be a count-down timer, so it only
updates once a day, but noticing a change helps to encourage the
desired reflection.

If the mode-line is too much, you could add it to your
`org-agenda-custom-commands` or similar.

## Usage

* Enable `memento-mode`.
* Look at the mode line and consider the impermanence of all things.
* Spend time doing the things you love (like using Emacs).
